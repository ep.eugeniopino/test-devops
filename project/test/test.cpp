#include <stdexcept>

bool test_function()
{
    return true;
}

void check(const bool arg)
{
    if (!arg)
        throw std::runtime_error("FAIL");
}

int main()
{
    check(test_function());
    return 0;
}
